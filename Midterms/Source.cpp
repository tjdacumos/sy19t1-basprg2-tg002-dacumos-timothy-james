#include <iostream>
#include <string>
#include <ctime>

using namespace std;

//card node  (struct)
struct cardNode
{
	string cardName;
	int cardValue; // for evaluation purposes
	cardNode* next = NULL;
};

//place bet function
void betInput(int wager, int&bet)
{
	cout << "Rate: 1 millimeters(mm) = 100000 yen. " << endl << endl;
	do
	{
		cout << "How many mm would like to wager, Kaiji? ";
		cin >> bet;
		//wager invalid
		if (bet > wager || bet <= 0)
		{
			cout << "Invalid wager! Input wager again!" << endl;
		}
	} while (bet > wager || bet <= 0);
	system("pause");
	system("cls");
}

//emperor or slave side
void selectSide(int owner, int round, string& side)
{
	if (round <= 3 || (round >= 7 && round <= 9))
	{
		if (owner == 0) //ai side
		{
			side = "Slave";
		}
		else if (owner == 1) //player side
		{
			side = "Emperor";
		}
	}
	else if ((round >= 4 && round <= 6) || round >= 10)
	{
		if (owner == 0) // ai side
		{
			side = "Emperor";
		}
		else if (owner == 1)//player side
		{
			side = "Slave";
		}
	}
}

//cards in hand
void cardHand(cardNode*&head, cardNode*&tail, string side, int size)
{
	//node adding card
	for (int i = 0; i < size; i++)
	{
		cardNode* temp = new cardNode;
		if (i==0)
		{
			if (side == "Emperor")
			{
				temp->cardName = "Emperor";
				temp->cardValue = 2;
			}
			else if (side == "Slave")
			{
				temp->cardName = "Slave";
				temp->cardValue = 0;
			}
		}
		else
		{
			temp->cardName = "Civilian";
			temp->cardValue = 1;
		}

		temp->next = NULL;

		if (head == NULL && tail == NULL)
		{
			head = temp;
			tail = temp;
		}
		else
		{
			tail->next = temp;
			tail = tail->next;
			tail->next = head;
		}
	}
}

//print cards
void cardPrint(cardNode* head, int size)
{
	cardNode* temp = head;
	cout << "Your cards are... (Input card number to select)" << endl;
	cout << "================================================" << endl;
	for (int i = 0; i < size; i++)
	{
		cout << "[" << i + 1 << "]" << temp->cardName << endl;
		temp = temp->next;
	}
}

//which card to play
void cardChoose(cardNode* head, int owner, cardNode*& holder, int&size)
{
	cardNode* temp = head;
	int cardPick;

	if (owner == 0) // ai turn
	{
		cardPick = rand() % size + 1;
		size--;
	}
	else //player turn
	{
		do
		{
			cin >> cardPick;
			if (cardPick > size)
			{
				cout << "Invalid pick/card!" << endl;
			}
		} while (cardPick > size);
	}

	for (int i = 1; i < cardPick; i++)
	{
		temp = temp->next;
	}
	
	holder = temp;

	if (owner == 1)
	{
		cout << "You choose " << holder->cardName << endl;
	}
	
}

//card compare and payout
void cardPayout(cardNode*& player, cardNode*& enemy,string side, int&cash, int&wager, int bet, bool&end)
{
	system("pause");
	system("cls");

	cout << "Open Cards!" << endl << endl;
	cout << "[Kaiji] " << player->cardName << " vs [Tonegawa] " << enemy->cardName << endl << endl;
	
	//draw condition
	if (player->cardValue == enemy->cardValue)
	{
		cout << "Draw!!!" << endl;
		system("pause");
		system("cls");
		return;
	}
	//if player Emperor
	if(side == "Emperor")
	{
		//lose condition
		if (player->cardValue == 2 && enemy->cardValue == 0) //emperor vs slave
		{
			cout << "You actually lost! It's drilling time! " << endl;
			cout << "The pin will now move by " << bet << "mm" << endl;
			cout << "zzzzz...zzzzz...zzzzzz...zzzzz" << endl;
			cout << "Kaiji: WAAAAAAAAAAAAAAHHHHHHHHHH!!!" << endl;
			wager = wager - bet;
		}
		// win condition
		else if (player->cardValue > enemy->cardValue) // emperor vs civilian
		{
			cout << "You win! \nKaiji: Easy money!" << endl;
			cout << "Cash won: " << bet * 100000 << "yen" << endl;
			cash = cash + (bet * 100000);
			
		}
	}
	//if player Slave
	if (side == "Slave")
	{
		//win condition
		if (player->cardValue == 0 && enemy->cardValue == 2) // slave vs emperor
		{
			cout << "You win! \nTonegawa: Nani!?!?" << endl;
			cout << "Cash won: " << bet * 500000 << "yen" << endl;
			cash = cash + (bet * 500000);
		}
		//lose condition
		else if (player->cardValue < enemy->cardValue) // slave vs civilian
		{
			cout << "You lost! It's drilling time! " << endl;
			cout << "The pin will now move by " << bet << "mm" << endl;
			cout << "zzzzz...zzzzz...zzzzzz...zzzzz" << endl;
			cout << "Kaiji: WAAAAAAAAAAAAAAHHHHHHHHHH!!!" << endl;
			wager = wager - bet;
		}
	}
	end = true;
	system("pause");
	system("cls");
}

//removing use card (delete function)
void cardRemove(cardNode*& head, cardNode* holder)
{
	cardNode* temp = holder;
	//last card
	if (head == temp)
	{
		return;
	}
	while (head->next != temp)
	{
		head = head->next;
	}
	head->next = temp->next;
	delete temp;
}

//play turns
void playTurn(int player, int enemy, string & playerSide, string & enemySide, int& cash, int& wager, int& bet)
{
	cardNode* playerHead = NULL;
	cardNode* playerTail = NULL;
	cardNode* enemyHead = NULL;
	cardNode* enemyTail = NULL;
	cardNode* playerCard = NULL;
	cardNode* enemyCard = NULL;
	int size = 5;
	bool end = false; // loop ender

	while (size != 0 && end == false)
	{
		//player turn
		cardHand(playerHead, playerTail, playerSide, size);
		cardPrint(playerHead, size);
		cardChoose(playerHead, player, playerCard, size);
		
		//enemy turn
		cardHand(enemyHead, enemyTail, enemySide, size);
		cardChoose(enemyHead, enemy, enemyCard, size);
		
		//results
		cardPayout(playerCard, enemyCard, playerSide, cash, wager, bet, end);
		
		cardRemove(playerHead, playerCard);
		cardRemove(enemyHead, enemyCard);	
	}
}

//evaluate ending
void endingEvaluate(int cash, int wager)
{
	//time to be very creative in showing the ending
	if (cash >= 20000000)
	{
		cout << "Best Ending!" << endl;
		cout << "=====================================" << endl;
		cout << "Congratulations you won " << cash << " yen!!!\nYou still have " << wager << "mm to spare!" << endl << endl;
		cout << "Tonegawa: NANI!?!?! B-B-B-BAKANA!!!" << endl;
		cout << "Kaiji: Yattaaa! GG EZ Tonegawa! :P " << endl;
	}
	else if (wager > 0)
	{
		cout << "Meh Ending!" << endl;
		cout << "=====================================" << endl;
		cout << "Congratulations! Your ear drums survived and you have " << cash << "yen.\nYou still have " << wager << "mm to spare!" << endl << endl;
		cout << "Tonegawa: Hmph (stands and walks away)" << endl;
		cout << "Kaiji: Hayyy! gg wp Tonegawa!" << endl;
	}
	else if (wager == 0)
	{
		cout << "Bad Ending!" << endl;
		cout << "=====================================" << endl;
		cout << "You lost!!! Your ear drums will be drilled!" << endl << endl;
		cout << "Tonegawa: MUAHAHAHAHHA!!!" << endl;
		cout << "Kaiji: ITAI!!! AHHHHHHHHHHHHHH!!! ITAI!!! ITAI!!! AHHHHHHHHHHHHHH!!! (You died -dark souls)" << endl;
	}
}

//play whole game
void playGame()
{
	string playerSide;
	string enemySide;
	int cash = 0;
	int wager = 30;
	int round = 1;
	int bet = 0;
	int player = 1;
	int enemy = 0;

	cout << "Welcome Kaiji to the EMPEROR CARD game! " << endl << endl;

	while (round < 13 && wager > 0 && cash < 20000000)
	{
		selectSide(player, round, playerSide);
		selectSide(enemy, round, enemySide);
		
		cout << "Cash: " << cash << endl;
		cout << "Distance left (mm): " << wager << endl;	
		cout << "=====================================" << endl;
		cout << "\tRound " << round << " out of 12" << endl;
		cout << "=====================================" << endl;
		cout << "Side: " << playerSide << endl << endl;
		betInput(wager, bet);
		//turns
		playTurn(player, enemy, playerSide, enemySide, cash, wager, bet);
		round++;		
	}
	endingEvaluate(cash, wager);
}
int main()
{
	srand(time(NULL));
	
	playGame();

	system("pause");
	return 0;
}