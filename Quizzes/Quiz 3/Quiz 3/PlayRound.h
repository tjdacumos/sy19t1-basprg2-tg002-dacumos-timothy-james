#pragma once

#include<string>
#include<iostream>

class Character;
using namespace std;

class PlayRound
{
public:
	PlayRound(Character* player, Character* enemy, int round);

	void compareAGI(Character * player, Character * enemy); // to check turn order
	void confirmHIT(Character * attacker, Character * defender);
	void combatDamage(Character * player, Character * enemy);
	void upgradeStats(Character * player, Character * enemy);
	void upgradeEnemy(Character* enemy, int round);
	void healPlayer(Character * player);
	void displayCombat(Character* attacker, Character* defender);
	
	
private:
		
	int mDamage;
	int mHitRate;
	bool mTurnOrder;
	bool mHitConfirm;
	Character* attacker;
	Character* defender;
	Character* temp;

	

};

