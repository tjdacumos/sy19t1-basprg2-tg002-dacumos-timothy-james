#include "Character.h"


Character::Character(string name, int select)
{
	if (select == 1)
	{
		// low hp and vit, mid dps, high dex and agi
		mName = name;
		mClass = "Assassin";
		mHpMax = rand() % 8 + 10;
		mHpCurr = mHpMax;
		mPower = rand() % 7 + 7;
		mVitality = rand() % 5 + 3;
		mAgility = rand() % 7 + 8;
		mDexterity = rand() % 7 + 9;
		mClassValue = 0;
	}
	else if (select == 2)
	{
		//mid hp and vit, high dps, low dex and ag
		mName = name;
		mClass = "Mage";
		mHpMax = rand() % 8 + 15;
		mHpCurr = mHpMax;
		mPower = rand() % 7 + 9;
		mVitality = rand() % 3 + 5;
		mAgility = rand() % 5 + 6;
		mDexterity = rand() % 5 + 7;
		mClassValue = 1;
	}
	else if (select == 3)
	{
		//high hp and vit, low dps, mid dexand agi
		mName = name;
		mClass = "Warrior";
		mHpMax = rand() % 10 + 20;
		mHpCurr = mHpMax;
		mPower = rand() % 5 + 7;
		mVitality = rand() % 5 + 7;
		mAgility = rand() % 5 + 4;
		mDexterity = rand() % 5 + 5;
		mClassValue = 2;
	}
}

void Character::takeDamage(int damage)
{
	mHpCurr -= damage;
	if (mHpCurr < 0)mHpCurr = 0;
}

void Character::healPlayer(int heal)
{
	mHpCurr += heal;
	if (mHpCurr >= mHpMax) mHpCurr = mHpMax;
}

void Character::updateStats(int maxHP, int pow, int vit, int dex, int agi)
{
	mHpMax += maxHP;
	mPower += pow;
	mVitality += vit;
	mAgility += agi;
	mDexterity += dex;
}

string Character::getName()
{
	return mName;
}

string Character::getClass()
{
	return mClass;
}

int Character::getHpMax()
{
	return mHpMax;
}

int Character::getHpCurr()
{
	
	return mHpCurr;
}

int Character::getVIT()
{
	return mVitality;
}

int Character::getPOW()
{
	return mPower;
}

int Character::getAGI()
{
	return mAgility;
}

int Character::getDEX()
{
	return mDexterity;
}

int Character::getClassValue()
{
	return mClassValue;
}

void Character::displayCharacter()
{
	cout << "Name: " << getName() << endl;
	cout << "Class: " << getClass() << endl;
	cout << "HP: " << getHpCurr() << "/" << getHpMax() << endl;
	cout << "Pow: " << getPOW() << endl;
	cout << "Vit: " << getVIT() << endl;
	cout << "Dex: " << getDEX() << endl;
	cout << "Agi " << getAGI() << endl;
}

void Character::displayStatsUp(int maxHP, int pow, int vit, int dex, int agi)
{
	cout << "HP: " << maxHP << endl;
	cout << "Pow: " << pow << endl;
	cout << "Vit: " << vit << endl;
	cout << "Dex: " << dex << endl;
	cout << "Agi: " << agi << endl;
}

