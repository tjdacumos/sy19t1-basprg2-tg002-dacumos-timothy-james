#include <iostream>
#include <string>
#include <ctime>
#include "PlayRound.h"
#include "Character.h"

using namespace std;

int main()
{
	srand(time(NULL));

	string name;
	string enemyNames[3] = {"Enemy Assassin", "Enemy Mage", "Enemy Warrior"};
	string selectName;
	int select;
	int stage = 0;

	cout << "Welcome to the Arena!!" << endl;
	cout << "Input your name: ";
	cin >> name;

	do
	{
		cout << "Choose your desired class: " << endl;
		cout << "=====================================" << endl;
		cout << "\t [1] Assassin (Strong against Mage. low hp and vit, mid dps, high dex and agi)" << endl;
		cout << "\t [2] Mage (Strong against Warrior. mid hp and vit, high dps, low dex and agi)" << endl;
		cout << "\t [3] Warrior (Strong against Asssassin. high hp and vit, low dps, mid dex and agi)" << endl;
		cin >> select;

		system("pause");
		system("cls");

	} while (select < 1 && select >3);
	
	Character* player = new Character(name, select);

	do
	{
		stage++;
		
		int enemySelect = rand() % 3 + 1;
		selectName = enemyNames[enemySelect-1];
		Character* enemy = new Character(selectName, enemySelect);
		

		cout << "Stage: " << stage << endl;
		cout << "=====================================" << endl;

		player->displayCharacter();

		cout << "==========================" << endl;
		cout << "||\t VS!! \t\t||" << endl;
		cout << "==========================" << endl;

		enemy->displayCharacter();

		system("pause");
		system("cls");

		PlayRound* start = new PlayRound(player, enemy, stage);

		delete start;
		delete enemy;

	}while (player->getHpCurr() != 0);
	
	system("cls");

	cout << "Game Over..." << endl;
	cout << "=====================================" << endl;
	cout << "You've reached stage: " << stage << endl;
	cout << "=====================================" << endl;
	cout << "Final Stats: " << endl;
	cout << "|||||||||||||||||" << endl;
	player->displayCharacter();

	system("pause");
	return 0;
}
