#pragma once
#include<string>
#include<iostream>

using namespace std;

class Character
{
public:
	
	Character(string name, int select);

	void takeDamage(int damage);
	void healPlayer(int heal);
	void updateStats(int maxHP, int pow, int vit, int agi, int dex);
	
	string getName();
	string getClass();

	int getHpMax();
	int getHpCurr();
	int getVIT();
	int getPOW();
	int getAGI();
	int getDEX();
	int getClassValue();
	
	void displayCharacter();
	void displayStatsUp(int maxHP, int pow, int vit, int agi, int dex);

private:
	string mClass;
	string mName;
	int mHpMax;
	int mHpCurr;
	int mPower;
	int mVitality;
	int mAgility;
	int mDexterity;
	int mClassValue; //for advantages
	


};

