#include "PlayRound.h"
#include "Character.h"

PlayRound::PlayRound(Character* player, Character* enemy, int round)
{
	mTurnOrder = true;
	upgradeEnemy(enemy, round);
	
	compareAGI(player, enemy);
	
	if (mTurnOrder == true) // player first
	{
		attacker = player;
		defender = enemy;
	}

	if(mTurnOrder == false) // enemy first
	{
		attacker = enemy;
		defender = player;
	}
	int turn = 1;
	int counter = 0;
	do
	{
	
		cout << "Turn: " << turn << endl;
		cout << "|||||||||||||||||||||||||" << endl;
		
	

		mHitConfirm = false;

		confirmHIT(attacker, defender);
	
		if (mHitConfirm == true)
		{
			combatDamage(attacker, defender);
		}

		displayCombat(attacker, defender);
		system("pause");
		
		counter++;
		if (counter % 2 == 0)
		{
			turn++;
			system("cls");
		}
		
		temp = attacker;
		attacker = defender;
		defender = temp;

	} while (attacker->getHpCurr() > 0 && defender->getHpCurr() >0);
	system("cls");
	if (player->getHpCurr() <= 0) return;
	
	cout << "Combat Ended! You're Victorious! " << endl;
	cout << "Here's your stat upgrades: " << endl;
	cout << "=====================================" << endl;

	upgradeStats(player, enemy);
	cout << endl;

	healPlayer(player);
	system("pause");
	system("cls");
}

void PlayRound::compareAGI(Character* player, Character* enemy)
{
	if (player->getAGI() < enemy->getAGI())
	{
		mTurnOrder = false;
	}
}

void PlayRound::confirmHIT(Character* attacker, Character* defender)
{
	//hit% = (DEX of attacker / AGI of defender) * 100
	int check = rand() % 100 + 1;

	mHitRate = ((attacker->getDEX() / defender->getAGI()) * 100);

	if (mHitRate > 80) mHitRate = 80;							// to check if will hit
	else if (mHitRate < 20) mHitRate = 20;
	if (check <= mHitRate) mHitConfirm = true;					// confirm the hit
}

void PlayRound::combatDamage(Character* attacker, Character* defender)
{
	//damage = (POW of attacker - VIT of defender) * bonusDamage
	mDamage = attacker->getPOW() - defender->getVIT();

	if (attacker->getClassValue() == 0 && defender->getClassValue() == 1) mDamage *= 1.5; // assassin vs mage
	if (attacker->getClassValue() == 1 && defender->getClassValue() == 2) mDamage *= 1.5; // mage vs warrior
	if (attacker->getClassValue() == 2 && defender->getClassValue() == 0) mDamage *= 1.5; // warrior vs assassin
	
	if (mDamage < 1) mDamage = 1;
	defender->takeDamage(mDamage);
}


void PlayRound::upgradeStats(Character* player, Character * enemy)
{
	if (enemy->getClassValue() == 0)
	{
		player->updateStats(0, 0, 0, 3, 3);
		player->displayStatsUp(0, 0, 0, 3, 3);
	}

	if (enemy->getClassValue() == 1)
	{
		player->updateStats(0, 5, 0, 0, 0);
		player->displayStatsUp(0, 5, 0, 0, 0);
	}
	if (enemy->getClassValue() == 2)
	{
		player->updateStats(3, 0, 3, 0, 0);
		player->displayStatsUp(3, 0, 3, 0, 0);
	}
}

void PlayRound::upgradeEnemy(Character* enemy, int round)
{
	if (round == 1) return;
	int up = rand() % 1+(round-1);
	enemy->updateStats(up, up, up, up, up);
}

void PlayRound::healPlayer(Character* player)
{
	int heal = player->getHpMax() * .3;
	player->healPlayer(heal);
	cout << "Congrats on your win. Here's a heal for you!" << endl;
	cout << "You healed for " << heal << " HP!" << endl;
}

void PlayRound::displayCombat(Character* attacker, Character* defender)
{
	if (mHitConfirm == false)
	{
		cout << "====================================" << endl;
		cout << attacker->getName() << " missed! " << endl;
		cout << "====================================" << endl;
		
	}
	else if (mHitConfirm == true)
	{
		
		cout << "==========================================" << endl;
		cout << attacker->getName() << " dealt " << mDamage << " damage to " << defender->getName() << "." << endl;
		cout << "==========================================" << endl;
		
	}

	cout << "Remaining HP of " << defender->getName() << " is " << defender->getHpCurr() << "/" << defender->getHpMax() << endl << endl;
}





