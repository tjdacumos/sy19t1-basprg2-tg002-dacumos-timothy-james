#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

void packageSort(int packages[]) 
{
	int temp = 0;

	for (int k = 0; k < 7; k++)
	{
		for (int l = k + 1; l < 7; l++)
		{
			if (packages[l] < packages[k])
			{
				temp = packages[k];
				packages[k] = packages[l];
				packages[l] = temp;
			}
		}
	}
}

void packagePrint(int packages[])
{
	packageSort(packages);

	for (int i = 0; i < 7; i++)
	{
		cout << "Package # " << i + 1 << ": " << packages[i] << endl;
	}
}

int packageSuggest(int itemPrice, int userGold, int packages[])
{
	int i = 0;
	bool check = false;
	string buy;

	packageSort(packages);

	while (i < 7 && check == false)
	{
		if ((packages[i] + userGold) >= itemPrice)
		{
			cout << "Avail new package!! \nThis is our suggested package: \nPackage #" << i + 1 << ": " << packages[i] << " gold" << endl;
			cout << "Type N or No if you don't want to avail package. (Enter any key if you want to buy) ";
			cin >> buy;
			cout << endl;
			if (buy == "No" || buy == "N")
			{
				cout << "You didn't buy the package... end of transaction" << endl;
				return userGold;
			}
			userGold = packages[i] + userGold - itemPrice;
			cout << "You bought Package #" << i + 1 << ": " << packages[i] << " gold" << endl;
			cout << "Item purchased!" << endl;
			check = true;
		}
		i++;
	}
	if (check == false)
	{
		cout << "Unfortunately, there's no package that can accomodate your purchase... "<< endl;
	}
	return userGold;
}

int priceCheck(int itemPrice, int userGold, int packages[])
{
	if (itemPrice <= userGold)
	{
		userGold = userGold - itemPrice;
		cout << "Item purchased!" << endl;
	}
	else if (itemPrice > userGold)
	{
		cout << "You don't have enough gold for the item!" << endl;
		userGold = packageSuggest(itemPrice, userGold, packages);
	}
	return userGold;
}

int main()
{
		int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
		int userGold = 250;
		int itemPrice;
		string cont;
		
		while (true)
		{
			packagePrint(packages);
			cout << "\nYou current Gold is: " << userGold << endl;
			cout << "Input price of item that you want to buy: ";
			cin >> itemPrice;
			cout << endl;

			userGold = priceCheck(itemPrice, userGold, packages);

			system("pause");
			system("cls");

			cout << "Your current gold balance is: " << userGold << endl;
			cout << "Do you want to continue shopping? (entering other keys/words means continue) (Y) Yes / (N) No: ";
			cin >> cont;
			if (cont == "No" || cont == "N")
			{
				break;
			}
			
		system("cls");
	}

	system("pause");
	return 0;
}