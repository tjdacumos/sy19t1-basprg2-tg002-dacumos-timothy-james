#include <iostream>
#include <string>
#include <ctime>
#include "Node.h"

using namespace std;

//Number of people 
int memberSize(int& size)
{
	cout << "The Night's Watch last hope!" << endl;
	cout << "How many Night's Watch people do you want to include?";
	cin >> size;

	return size;
}

//aadding names(node) in the list
void addMember(Node* &head, Node* &tail, string nameInput)
{
	Node* temp = new Node;
	
	temp->name = nameInput;
	temp->next = NULL;

	if (head == NULL && tail == NULL)
	{
		head = temp;
		tail = temp;
	}
	else
	{
		tail->next = temp;
		tail = tail->next;
		tail->next =head;
	}
}

//Arrangment of people and names (circular)
void memberNames(Node*&start, Node* &last, int size)
{
	string peopleName;

	for (int i = 0; i < size; i++)
	{
		//input names
		cout << "Enter Night's Watch member names: ";
		cin >> peopleName;
		//create circular link list
		addMember(start, last, peopleName);
	}
	
}

//display names
void printNames(Node* head, int size)
{
	Node* temp = head;
	cout << "People Remaning: " << endl;
	
	for (int i = 0; i < size; i++)
	{
		cout << temp->name << endl;
		temp = temp->next;
	}
	
}

//random number selection
int randomNumber(int size, int& roll)
{
	srand(time(NULL));

	roll = rand() % size + 1;

	return roll;
}

//starting point of the cloak
void memberStartPt(Node*& head, Node* tail, int roll)
{
	for (int i = 1; i < roll; i++)
	{
		head = (*head).next;
	}
	cout << (*head).name << " will start the random selection!" << endl;
}


//who will be the one to get back and defend the wall
Node*memberCloak(Node*& head, Node*& tail, int roll)
{
	Node* remove = head;

	cout << (head)->name << " drew " << roll << "!" << endl;
	//passing of cloak
	for (int i = 0; i < roll; i++)
	{
		remove = (*remove).next;
	}
	
	cout << remove->name << " was drawn! He/she went back to defend the Wall!" << endl;
	return remove;
}


//remove player from the list
void memberRemove(Node*& head, Node*& tail, int roll)
{
	Node* temp = head;
	tail = memberCloak(head, tail, roll);

	if ((head)->name == (tail)->name)
	{
		head = (head)->next;
		(tail)->next = head;
		temp->next = NULL; 
	}
	else
	{
		while ((head)->next != tail)
		{
			head = (head)->next;
		}
	}
	(tail)->next = NULL;
	temp->next = head;
	tail = temp;
}

//winning player
void memberWinner(Node* list)
{
	cout << "========================================" << endl;
	cout << "\t\tOur Only Hope!!" << endl;
	cout << "========================================" << endl;
	cout << "Go now! " << " You're the last hope we have now" << (*list).name<< endl;
}

//Play everything
void playRound()
{
	Node* start = NULL;
	Node* last = NULL;
	int size = 0;
	int roll;
	int round = 1;
	

	memberSize(size);
	memberNames(start, last, size);
	system("pause");
	system("cls");

	randomNumber(size, roll);
	cout << "Roll" << roll;
	memberStartPt(start, last, roll);
	while (size != 1)
	{
		
		cout << "========================================" << endl;
		cout << "\t\tRound " << round << endl;
		cout << "========================================" << endl;
		printNames(start, size);
		randomNumber(size, roll);
		memberRemove(start, last,roll);
		round++;
		size--;
		system("pause");
		system("cls");
	}
	memberWinner(start);
}
int main()
{

	playRound();
	
	system("pause");
	return 0;
}

//Note to self: improve this code once you get better both mentally and physically.