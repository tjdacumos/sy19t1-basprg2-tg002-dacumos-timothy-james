#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Item.h"
#include "Inventory.h"
#include <ctime>



class Item;

using namespace std;

class Player
{
public:
	Player(string name, const Inventory & inventory);

	//Getters
	string getName();
	Inventory&getInventory();
	int getCurrentHp();
	void printItems(Player*player);
	void printCollated(Player* player);
	const vector<Item*>& getItems();

	void heal(int amount);
	void damage(int amount);
	void addItem(Item* item);

	void displayStatus();

private:
	string mName;
	Inventory mInventory;
	int mHp;
	vector<Item*> mItems;
	int ssr = 0, sr = 0, r = 0, p = 0, b = 0, c = 0;
};

