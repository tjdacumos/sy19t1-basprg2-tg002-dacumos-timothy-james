#include "Bomb.h"

Bomb::Bomb(string name, int damage)
	:Item(name)
{
	mDamage = damage;
}

void Bomb::gachaPull(Player* owner)
{
	Item::gachaPull(owner);
	
	owner->damage(mDamage);
	setEffect("25 damage to your HP!");
	cout << "You pulled " << getName() << endl;
	cout << "You received " << getEffect() << endl;
}
