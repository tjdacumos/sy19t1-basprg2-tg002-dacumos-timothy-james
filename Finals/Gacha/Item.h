#pragma once
#include "Player.h"

using namespace std;

class Player;

class Item
{
public:
	Item(string name);
	~Item();

	//getters
	string getName();
	string getEffect();
	Player* getOwner();

	//setter
	void setEffect(string effect);
	void setOwner(Player* owner);

	virtual void gachaPull(Player* owner);

private:
	string mName;
	string mEffect;
	Player* mOwner;

};
