#include "RarityPoints.h"

RarityPoints::RarityPoints(string name, int type)
	:Item (name)
{
	mType = type;
}

void RarityPoints::gachaPull(Player* owner)
{
	Item::gachaPull(owner);

	if (mType == 3)
	{
		mAdd = 50;
		owner->getInventory().rarityPts += mAdd;
		setEffect("50 Rarity Points");
	}
	if (mType == 2)
	{
		mAdd = 10;
		owner->getInventory().rarityPts += mAdd;
		setEffect("10 Rarity Points");
	}
	if (mType == 1)
	{
		mAdd = 1;
		owner->getInventory().rarityPts += mAdd;
		setEffect("1 Rarity Points");
	}
	cout << "You pulled " << getName() << endl;
	cout << "You received " << getEffect() << endl;
}
