#include "Player.h"

Player::Player(string name, const Inventory& inventory)
{
	mName = name;
	mInventory = inventory;
	mHp = 100;
}

string Player::getName()
{
	return mName;
}

Inventory& Player::getInventory()
{
	return mInventory;
}

int Player::getCurrentHp()
{
	return mHp;
}

void Player::printItems(Player * player)
{
	int count = 1;

	for (int i = 0; i < mItems.size(); i++)
	{
		cout << count<< ". ";
		cout << player->getItems()[i]->getName() << "  ";
		count++;
	}
}

void Player::printCollated(Player* player) 
{

	for (int i = 0; i < player->getItems().size(); i++)
	{
		
		if (player->getItems()[i]->getName() == "SSR") ssr++;
		if (player->getItems()[i]->getName() == "SR") sr++;
		if (player->getItems()[i]->getName() == "R") r++;
		if (player->getItems()[i]->getName() == "Health Potion") p++;
		if (player->getItems()[i]->getName() == "Bomb") b++;
		if (player->getItems()[i]->getName() == "Crystal") c++;
	}
	cout << "SSR x" << ssr << endl;
	cout << "SR x" << sr << endl;
	cout << "R x" << r << endl;
	cout << "Heatlh Potion x" << p << endl;
	cout << "Bomb x" << b << endl;
	cout << "Crystals x" << c << endl;
}

const vector<Item*>& Player::getItems()
{
	return mItems;
}

void Player::heal(int amount)
{
	mHp += amount;
}

void Player::damage(int amount)
{
	if (amount > mHp) mHp = 0;
	mHp -= amount;
}

void Player::addItem(Item * item)
{
	item->setOwner(this);

	mItems.push_back(item);
}



void Player::displayStatus()
{
	cout << "HP: " << mHp << endl;
	cout << "Crystal: " << mInventory.crystals << endl;
	cout << "Rarity Points: " << mInventory.rarityPts << endl;
}
