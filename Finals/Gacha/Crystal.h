#pragma once
#include "Item.h"

class Player;

class Crystal : 
	public Item
{
public:
	Crystal(string name, int add);

	void gachaPull(Player* owner);

private:
	int mAdd; // inventory addition
};

