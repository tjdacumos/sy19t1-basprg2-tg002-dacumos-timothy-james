#pragma once
#include "Item.h"

class Player;

class Bomb :
	public Item
{
public:
	Bomb(string name, int damage);

	void gachaPull(Player* owner);
private:
	int mDamage;
};


