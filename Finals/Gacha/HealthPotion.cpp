#include "HealthPotion.h"

HealthPotion::HealthPotion(string name, int heal)
	:Item(name)
{
	mHeal = heal;
}

void HealthPotion::gachaPull(Player* owner)
{
	Item::gachaPull(owner);
	setEffect("Healed 30 HP");
	owner->heal(mHeal);

	cout << "You pulled " << getName() << endl;
	cout << "You " << getEffect() << endl;
}
