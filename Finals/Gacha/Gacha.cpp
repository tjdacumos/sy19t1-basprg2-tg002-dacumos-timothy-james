#include <string>
#include <iostream>
#include <vector>
#include "Item.h"
#include "Inventory.h"
#include <ctime>
#include "Bomb.h"
#include "Crystal.h"
#include "HealthPotion.h"
#include "RarityPoints.h"
#include "Player.h"


using namespace std;

void pullFromGacha(Player* player)
{
	//deduct crsytal to pull
	player->getInventory().crystals -= 5;
	if (player->getInventory().crystals == 0) player->getInventory().crystals = 0;
	
	int r = rand() % 100 + 1;
	
	if (r == 1)
	{
		Item* SSR = new RarityPoints("SSR", 3);
		player->addItem(SSR);
		SSR->gachaPull(player);
	}
	else if (r <= 10)
	{
		Item* SR = new RarityPoints("SR", 2);
		player->addItem(SR);
		SR->gachaPull(player);
	}
	else if (r <= 50)
	{
		Item* R = new RarityPoints("R", 1);
		player->addItem(R);
		R->gachaPull(player);
	}
	else if (r <= 65)		{
		Item* Potion = new HealthPotion("Health Potion", 30);
		player->addItem(Potion);
		Potion->gachaPull(player);
	}
	else if (r <= 85)
	{
		Item* bombs = new Bomb("Bomb", 25);
		player->addItem(bombs);
		bombs->gachaPull(player);
	}
	else if (r <= 100)
	{
		Item* crystal = new Crystal("Crystal", 1);
		player->addItem(crystal);
		crystal->gachaPull(player);
	}
}

int main()
{
	srand(time(NULL));
	
	Inventory inventory;
	inventory.crystals = 100;
	inventory.rarityPts = 0;

	Player * player = new Player("Player", inventory);
	int pull = 1;

	while (player->getCurrentHp() > 0 && player->getInventory().rarityPts < 100 && player->getInventory().crystals >0)
	{
		cout << "==================================================" << endl;
		player->displayStatus();
		cout << "Pull: " << pull << endl;
		cout << endl;
		cout << "==================================================" << endl;
		system("Pause");
		cout << "+++++++++++++++++++++++++++++++" << endl;
		pullFromGacha(player);
		cout << "+++++++++++++++++++++++++++++++" << endl;
		pull++;
		system("pause");
		system("cls");
	}
	//endingEvaluation
	if (player->getCurrentHp() <= 0) cout << "You lose!! Sadly you depleted your hp!" << endl;
	if (player->getInventory().crystals <= 0) cout << "You lose!! You ran out of crystals! Nice try though" << endl;
	if (player->getInventory().rarityPts >= 100) cout << "Congratulations! You win!!" << endl;

	cout << "==================================================" << endl;
	cout << "Here's the complete list of items your aquire per pull: " << endl;
	player->printItems(player);

	cout << "Press any button to see the collated/game summary items!" << endl;

	system("Pause");
	system("cls");

	cout << "Game Summary " << endl;
	player->printCollated(player);
	
	system("Pause");
	return 0;
}

