#include "Item.h"

Item::Item(string name)
{
	mName = name;
}

string Item::getName()
{
	return mName;
}

string Item::getEffect()
{
	return mEffect;
}

Player* Item::getOwner()
{
	return mOwner;
}

void Item::setEffect(string effect)
{
	mEffect = effect;
}

void Item::setOwner(Player* owner)
{
	mOwner = owner;
}

void Item::gachaPull(Player* owner)
{
	if (mOwner == NULL) cout << "No one is playing the Gacha game yet" << endl;
}
