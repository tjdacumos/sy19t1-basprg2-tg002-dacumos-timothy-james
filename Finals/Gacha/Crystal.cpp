#include "Crystal.h"


Crystal::Crystal(string name, int add)
	:Item(name)
{
	mAdd = add; // amount of additional crystals
	
}

void Crystal::gachaPull(Player* owner)
{
	Item::gachaPull(owner);
	owner->getInventory().crystals+= mAdd ;
	setEffect("15 additional crystals");

	cout << "You pulled " << getName() << endl;
	cout << "You received " << getEffect() << endl;
}
