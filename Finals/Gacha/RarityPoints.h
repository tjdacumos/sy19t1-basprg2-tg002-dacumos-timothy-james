#pragma once
#include "Item.h"

class Player;

class RarityPoints :
	public Item
{
public:
	RarityPoints(string name, int type);

	void gachaPull(Player* owner);
private:
	int mType; // SSR(3), SR(2), R(1)
	int mAdd; // inventory addition
};

