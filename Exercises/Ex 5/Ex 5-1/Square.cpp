#include "Square.h"

Square::Square(float length)
	:Shape("Square", 4)
{
	mLength = length;
}


float Square::getArea()
{
	return mLength * mLength;
}
