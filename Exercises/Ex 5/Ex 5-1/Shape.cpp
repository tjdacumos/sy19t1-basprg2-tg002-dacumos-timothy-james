#include "Shape.h"



Shape::Shape(string name, int NumSides)
{
	mName = name;
	mNumSides = NumSides;
}

string Shape::getName()
{
	return mName;
}

int Shape::getNumSides()
{
	return mNumSides;
}
