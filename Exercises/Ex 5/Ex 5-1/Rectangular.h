#pragma once
#include "Shape.h"


class Rectangular : public Shape
{
public:
	Rectangular(float length, float width);

	float getArea() override;

private:
	float mLength;
	float mWidth;
};

