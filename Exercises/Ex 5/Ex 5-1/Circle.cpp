#include "Circle.h"

Circle::Circle(float radius)
	:Shape("Circle", 0)
{
	mRadius = radius;
}


float Circle::getArea()
{
	return 3.14 * mRadius * mRadius;
}
