#pragma once
#include<string>

using namespace std;

class Shape
{
public:
	Shape(string name, int NumSides);
	~Shape();

	string getName();
	int getNumSides();
	virtual float getArea() = 0;

private:
	string mName;
	int mNumSides;

};