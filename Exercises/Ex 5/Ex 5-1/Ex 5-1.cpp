#include <iostream>
#include <string>
#include <vector>
#include "Circle.h"
#include "Square.h"
#include "Rectangular.h"

using namespace std;

void printShapes(const vector<Shape*>& shapes)
{
	for (int i = 0; i < shapes.size(); i++)
	{
		Shape* shape = shapes[i];
		cout << shape->getName() << ":\nNumber of Sides: " << shape->getNumSides() << endl;
		cout << "Area: " << shape->getArea() << endl << endl;
	}
}
int main()
{
	vector<Shape*> shapes;

	Circle* circle = new Circle(5.5f);
	shapes.push_back(circle);

	Square* square = new Square(6.5f);
	shapes.push_back(square);

	Rectangular* rectangular = new Rectangular(7.5f, 8.5f);
	shapes.push_back(rectangular);

	printShapes(shapes);

	system("pause");
	return 0;
}