#include "Rectangular.h"

Rectangular::Rectangular(float length, float width)
	:Shape("Rectangular", 4)
{
	mLength = length;
	mWidth = width;
}

float Rectangular::getArea()
{
	return mWidth * mLength;
}
