#pragma once
#include "Unit.h"
class Concentration :
	public Unit
{
public:
	Concentration();

	string buffName() override;
	string buffEffect() override;
	void buffUnit(Unit* target) override;
};

