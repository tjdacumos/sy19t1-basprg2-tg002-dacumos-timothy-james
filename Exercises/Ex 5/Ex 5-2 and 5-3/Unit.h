#pragma once
#include <string>
#include <iostream>

using namespace std;

class Unit
{
public:
	Unit();

	void displayStats();

	void healHpCurr(float heal);
	void addmPower(float buff);
	void addmVitality(float buff);
	void addDexterity(float buff);
	void addAgility(float buff);

	virtual string buffName();
	virtual string buffEffect();
	virtual void buffUnit(Unit* target);

private:
	string mUnitName;
	float mHpMax;
	float mHpCurr;
	float mPower;
	float mVitality;
	float mDexterity;
	float mAgility;
};