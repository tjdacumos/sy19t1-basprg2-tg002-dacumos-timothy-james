#include "Unit.h"

Unit::Unit()
{
	mHpCurr = 2;
	mHpMax = 32;
	mPower = 8;
	mVitality = 6;
	mDexterity = 8;
	mAgility = 10;
}

void Unit::displayStats()
{

	cout << "Player Stats: " << endl;
	cout << "HP: " << mHpCurr << "/" << mHpMax << endl;
	cout << "Power: " << mPower << endl;
	cout << "Vitality: " << mVitality << endl;
	cout << "Dexterity: " << mDexterity << endl;
	cout << "Agility: " << mAgility << endl;
}

void Unit::healHpCurr(float heal)
{
	mHpCurr += heal;
	if (mHpCurr > mHpMax)
	{
		mHpCurr = mHpMax;
	}
}

void Unit::addmPower(float buff)
{
	mPower += buff;
}

void Unit::addmVitality(float buff)
{
	mVitality += buff;
}

void Unit::addDexterity(float buff)
{
	mDexterity += buff;
}

void Unit::addAgility(float buff)
{
	mAgility += buff;
}

string Unit::buffName()
{
	return "Activate a buff";
}

string Unit::buffEffect()
{
	return "No Buffs activated";
}

void Unit::buffUnit(Unit* target)
{
	cout << "Activate buff and choose target" << endl;
}
