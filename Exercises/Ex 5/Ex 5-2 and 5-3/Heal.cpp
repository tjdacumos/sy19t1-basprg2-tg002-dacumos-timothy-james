#include "Heal.h"

Heal::Heal()
{
}

string Heal::buffName()
{
	return "Heal";
}

string Heal::buffEffect()
{
	return "Heals HP by 10 points";
}

void Heal::buffUnit(Unit* target)
{
	target->healHpCurr(10);
}
