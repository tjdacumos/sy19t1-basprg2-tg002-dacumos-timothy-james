#pragma once
#include "Unit.h"
class IronSkin :
	public Unit
{
public:
	IronSkin();

	string buffName() override;
	string buffEffect() override;
	void buffUnit(Unit* target) override;
};

