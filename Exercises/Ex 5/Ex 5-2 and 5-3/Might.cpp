#include "Might.h"

Might::Might()
{
}

string Might::buffName()
{
	return "Might";
}

string Might::buffEffect()
{
	return "Increases Power by 2";
}

void Might::buffUnit(Unit* target)
{
	target->addmPower(2);
}
