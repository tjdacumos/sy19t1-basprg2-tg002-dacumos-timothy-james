#pragma once
#include "Unit.h"
class Haste :
	public Unit
{
public:
	Haste();

	string buffName() override;
	string buffEffect() override;
	void buffUnit(Unit* target) override;
};

