#include "IronSkin.h"

IronSkin::IronSkin()
{
}

string IronSkin::buffName()
{
	return "Iron Skin";
}

string IronSkin::buffEffect()
{
	return "Increases Vitality by 2";
}

void IronSkin::buffUnit(Unit* target)
{
	target->addmVitality(2);
}
