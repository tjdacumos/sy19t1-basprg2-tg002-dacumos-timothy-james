#pragma once
#include "Unit.h"
class Heal :
	public Unit
{
public:
	Heal();

	string buffName() override;
	string buffEffect() override;
	void buffUnit(Unit* target) override;
};

