#include "Haste.h"

Haste::Haste()
{
}

string Haste::buffName()
{
	return "Haste";
}

string Haste::buffEffect()
{
	return "Increases Dexterity by 2";
}

void Haste::buffUnit(Unit* target)
{
	target->addAgility(2);
}
