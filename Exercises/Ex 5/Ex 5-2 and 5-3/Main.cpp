#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include "Concentration.h"
#include "Haste.h"
#include "Heal.h"
#include "IronSkin.h"
#include "Might.h"


using namespace std;

void printBuffs(const vector<Unit*>& buffs, Unit* target)
{
	int r = rand() % buffs.size();

	Unit* buff = buffs[r];
	cout << "Player cast: " << buff->buffName() << endl;
	cout << "Effect: " << buff->buffEffect() << endl;
	buff->buffUnit(target);

}
int main()
{
	srand(time(NULL));

	vector<Unit*> buffs;

	Heal* heal = new Heal();
	buffs.push_back(heal);

	Might* might = new Might();
	buffs.push_back(might);

	IronSkin* ironskin = new IronSkin();
	buffs.push_back(ironskin);

	Concentration* concentration = new Concentration();
	buffs.push_back(concentration);

	Haste* haste = new Haste();
	buffs.push_back(haste);

	Unit* player = new Unit();
	
	

	while (true)
	{
		cout << "======================================" << endl;
		cout << "Initial Player Stats: " << endl;
		player->displayStats();
		system("pause");
		cout << "======================================" << endl;
		cout << "|||||||||||||||||||||||||||||||" << endl;
		printBuffs(buffs, player);
		cout << "|||||||||||||||||||||||||||||||" << endl;
		system("pause");
		cout << "======================================" << endl;
		cout << "New Player stats: " << endl;
		player->displayStats();
		system("pause");
		system("cls");
	}
	system("pause");
	return 0;
}