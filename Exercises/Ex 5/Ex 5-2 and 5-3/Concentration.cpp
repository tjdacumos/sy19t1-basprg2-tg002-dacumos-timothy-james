#include "Concentration.h"

Concentration::Concentration()
{
}

string Concentration::buffName()
{
	return "Concentration";
}

string Concentration::buffEffect()
{
	return "Increases Dexterity by 2";
}

void Concentration::buffUnit(Unit* target)
{
	target->addDexterity(2);
}
