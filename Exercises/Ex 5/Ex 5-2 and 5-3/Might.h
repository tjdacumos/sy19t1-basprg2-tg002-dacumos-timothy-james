#pragma once
#include "Unit.h"
class Might :
	public Unit
{
public:
	Might();

	string buffName() override;
	string buffEffect() override;
	void buffUnit(Unit* target) override;
};

