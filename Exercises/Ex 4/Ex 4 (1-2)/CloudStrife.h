#pragma once
#include<string>

using namespace std;

class CloudStrife
{
public:
	CloudStrife();
	~CloudStrife();

	void viewStatus();
	void viewStats();
	void viewSkills();
	void viewInventory();

	string name;
	string skills;
	string weapon;
	string arm;
	string acc;
	int level;
	int mHP;
	int mMP;
	int currentHP;
	int currentMP;
	int exp;
	int strength;
	int dexterity;
	int vitality;
	int magic;
	int spirit;
	int luck;
	int attack;
	int attackChance;
	int defense;
	int defenseChance;
	int magicAtk;
	int magicDef;
	int magicDefchance;
};

