#include "CloudStrife.h"
#include<iostream>
#include<string>

//to satisfy the exercise only version
CloudStrife::CloudStrife() // never do this kind of hard coding. 
{
	name = "Cloud Strife";
	skills = "4x-Cut\nMagic\nD.blow\W-Item";
	weapon = "Ultima Weapon";
	arm = "Wizard Bracelet";
	acc = "Touph Ring";
	level = 99;
	mHP = 8759;
	mMP = 920;
	currentHP = 5935;
	currentMP = 877;
	exp = 5944665;
	strength = 253;
	dexterity = 255;
	vitality = 143;
	magic = 173;
	spirit = 181;
	luck = 197;
	attack = 255;
	attackChance = 110;
	defense = 149;
	defenseChance = 66;
	magicAtk = 173;
	magicDef = 181;
	magicDefchance = 3;
}

CloudStrife::~CloudStrife() // idk wtf is this for (ask sir what is this for)
{
}

void CloudStrife::viewStatus()
{
	cout << name << endl;
	cout << "LV " << level << endl;
	cout << "HP " << currentHP << "/" << mHP << endl;
	cout << "MP " << currentMP << "/" << mMP << endl;
	cout << "EXP\t" << exp << "p" << endl;
	cout << "=============================" << endl;
}

void CloudStrife::viewStats()
{
	cout << "Stats" << endl;
	cout << "=============================" << endl;
	cout << "Strength\t" << strength << endl;
	cout << "Dexterity\t" << dexterity << endl;
	cout << "Vitality\t" << vitality << endl;
	cout << "Magic\t\t" << magic << endl;
	cout << "Spirit\t\t" << spirit << endl;
	cout << "Luck\t\t" << luck << endl;
	cout << "Attack\t\t" << attack << endl;
	cout << "Attack%\t\t" << attackChance << endl;
	cout << "Defense\t\t" << defense << endl;
	cout << "Defense%\t" << defenseChance << endl;
	cout << "Magic atk\t" << magicAtk << endl;
	cout << "Magic def\t" << magicDef << endl;
	cout << "Magic def%\t" << magicDefchance << endl;
	cout << "=============================" << endl;
}

void CloudStrife::viewSkills()
{
	cout << "Skill" << endl;
	cout << "=============================" << endl;
	cout << skills << endl;
	cout << "=============================" << endl;
}

void CloudStrife::viewInventory()
{
	cout << "Inventory" << endl;
	cout << "=============================" << endl;
	cout << "Wpn:\t" << weapon << endl;
	cout << "Arm:\t" << arm << endl;
	cout << "Acc:\t" << acc << endl;
	cout << "=============================" << endl;
}




