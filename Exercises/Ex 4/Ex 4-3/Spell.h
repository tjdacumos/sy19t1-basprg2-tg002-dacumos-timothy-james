#pragma once
#include <string>

using namespace std;

class Spell
{
public:
	Spell();

	int castSpell();
	int manaSpell();
	void updateManaSpell(int x);
	
	string spellName;
	int damage;
	int costMP;

};

