#include "Spell.h"
#include "wizard.h"
#include <ctime>
#include <iostream>

using namespace std;

int main()
{
	srand(time(NULL));
	
	string p1, p2;

	cout << "Enter Player 1 name: ";
	cin >> p1;
	cout << "Enter Player 2 name: ";
	cin >> p2;

	system("pause");
	system("cls");

	Wizard w1 = Wizard(p1, 100, 100);
	Wizard w2 = Wizard(p2, 100, 100);

	w1.updateWizManaSpell(5);
	w2.updateWizManaSpell(5);

	cout << p1 << " VS " << p2 << endl;
	cout << p1 << " Initial stats: " << endl;
	cout << "=============================" << endl;
	w1.viewStatus();
	cout << "=============================" << endl;
	cout << p2 << " Initial stats: " << endl;
	cout << "=============================" << endl;
	w2.viewStatus();
	cout << "=============================" << endl;

	system("pause");
	system("cls");

	while (w1.playerHP > 0 && w2.playerHP > 0 && w1.playerMP > 0 && w2.playerMP > 0)
	{
		cout << p1 << "'s Turn" << endl;
		cout << "=============================" << endl;
		cout << "Current HP: " << w1.playerHP << endl;
		cout << "Current MP: " << w1.playerMP << endl;
		w2.updateHP(w2.targetHP() - w1.returnSpellDamage());
		w1.updateMP(w1.playerMP- w1.returnMPCost());
		cout << "Your attack cost " << w1.returnMPCost() << " MP and your new MP is: " << w1.playerMP << endl;
		cout << p2 << "'s Updated HP: " << w2.playerHP << endl;
		cout << "=============================" << endl << endl << endl;

		cout << p2 << "'s Turn" << endl;
		cout << "=============================" << endl;
		cout << "Current HP: " << w2.playerHP << endl;
		cout << "Current MP: " << w2.playerMP << endl;
		w1.updateHP(w1.targetHP() - w2.returnSpellDamage());
		w2.updateMP(w2.playerMP - w2.returnMPCost());
		cout << "Your attack cost " << w2.returnMPCost() << " MP and your new MP is: " << w2.playerMP << endl;
		cout << p1 << "'s Updated HP: " << w1.playerHP << endl;
		cout << "=============================" << endl;
		
		system("pause");
		system("cls");
	}

	if (w1.playerHP > w2.playerHP)
	{
		cout << "Wizard " << p1 << " won!" << endl;
	}
	else
	{
		cout << "Wizard " << p2 << " won!" << endl;
	}

	system("pause");
	return 0;
}