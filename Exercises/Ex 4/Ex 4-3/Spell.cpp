#include "Spell.h"
#include "wizard.h"
#include <iostream>

using namespace std;

Spell::Spell()
{
	spellName = "Fireball";
	
}

int Spell::castSpell()
{
	damage = rand() % 10 + 1;
	
	cout << "You casted " << spellName << " and did " << damage << " damage!" << endl;

	return damage;
}

int Spell::manaSpell()
{
	return costMP;
}

void Spell::updateManaSpell(int x)
{
	costMP = x;
}

