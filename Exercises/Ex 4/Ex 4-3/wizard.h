#pragma once
#include<string>
#include "Spell.h"

using namespace std;

class Wizard
{
public:
	Wizard(string name, int currentHP, int currentMP);

	void viewStatus();
	void updateHP(int x);
	void updateMP(int x);
	void updateWizManaSpell(int x);
	int returnSpellDamage();
	int returnMPCost();
	int targetHP();
	
	string playerName;
	int playerHP;
	int playerMP;
	Spell wizSpell;
	
};

