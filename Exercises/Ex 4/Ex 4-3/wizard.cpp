#include "wizard.h"
#include "Spell.h"
#include<string>
#include<iostream>

using namespace std;

Wizard::Wizard(string name, int currentHP, int currentMP)
{
	playerName = name;
	playerHP = currentHP;
	playerMP = currentMP;
	wizSpell = Spell();
}

void Wizard::viewStatus()
{
	cout << "Name = " << playerName << endl;
	cout << "HP = " << playerHP << endl;
	cout << "MP = " << playerMP << endl;
}

void Wizard::updateHP(int x)
{
	playerHP = x;
}

void Wizard::updateMP(int x)
{
	playerMP = x;
}

int Wizard::returnSpellDamage()
{
	return wizSpell.castSpell(); // damage
}

int Wizard::returnMPCost()
{
	return wizSpell.manaSpell(); // cost
}

void Wizard::updateWizManaSpell(int x)
{
	wizSpell.updateManaSpell(x);
}

int Wizard::targetHP()
{
	return playerHP;
}