#include <iostream>
#include <string>

using namespace std;

void arrayPrint(string items[], int size) 
{
	for (int i = 0; i < size; i++)
	{
		cout << items[i] << endl;
	}
}

int main()
{
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };

	arrayPrint(items, 8);
	
	system("pause");
	return 0;
}
