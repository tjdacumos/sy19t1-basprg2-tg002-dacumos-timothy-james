#include <iostream>
#include <ctime>

using namespace std;

void arrayLargest(int numbers[])
{
	for (int i = 0; i < 10; i++)
	{
		if (numbers[0] < numbers[i])
		{
			numbers[0] = numbers[i];
		}
	}
	cout << numbers[0] << endl;
}

void random(int arrayRandom[], int quantity)
{
	int x;

	for (int i = 0; i < quantity; i++)
	{
		x = rand() % 100 + 1;
		arrayRandom[i] = x;
	}
	arrayLargest(arrayRandom);
}

int main()
{
	srand(time(NULL));
	int array[10];

	random(array, 10);
	
	system("pause");
	return 0;
}
