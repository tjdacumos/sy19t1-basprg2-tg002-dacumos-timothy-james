#include <iostream>
#include <string>
using namespace std;

int factorial(int x)
{
	int result = 1;
	for (int counter = 1; counter <= x; counter++)
	{
		result = result * counter;
	}
	return result;
}

void inputCheck(int input) // to check user's input value if it's zero, negative or postive integer.
{
	if (input < 0)
	{
		cout << "Input positive integers only!" << endl;
	}
	else if (input > 0)
	{
		int answer = factorial(input);

		cout << "The result is: " << answer << endl;
	}
	else
	{
		cout << "The result is: 1" << endl;
	}
}


int main()
{
	int input;
	cout << "Input Factorial Number: ";
	cin >> input;

	inputCheck(input);

	system("pause");
	return 0;
}