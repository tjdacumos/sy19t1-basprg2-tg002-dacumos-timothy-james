#include <iostream>
#include <string>

using namespace std;

void itemInstances(string search, string items[])
{
	int noi = 0;
	bool found = false;

	for (int i = 0; i <= 8; i++)
		if (search == items[i])
		{
			noi++;
			found = true;
		}

	if (found == false)
	{
		cout << "Item " << search << " does not exist!" << endl;
	}
	else
	{
		cout << "You have " << noi << " " << search << endl;
	}
}

int main()
{
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	string input;

	cout << "Enter item: ";
	getline(cin, input);

	itemInstances(input, items);

	system("pause");
	return 0;
}
