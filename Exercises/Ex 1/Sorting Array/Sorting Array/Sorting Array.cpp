#include <iostream>
#include <ctime>

using namespace std;

void arrayRandom(int arrayRandom[]) 
{
	int x;

	cout << "Unsorted array: (randomly generated)" << endl;

	for (int i = 0; i < 10; i++)
	{
		x = rand() % 100 + 1;
		arrayRandom[i] = x;
		cout << arrayRandom[i] << " ";
	}
	cout << endl;
}

void arraySort(int array[])
{
	arrayRandom(array);
	int temp = 0;

	cout << "Sorted array: " << endl;

	for (int k = 0; k < 10; k++)
	{
		for (int l = k + 1; l < 10; l++)
		{
			if (array[l] < array[k])
			{
				temp = array[k];
				array[k] = array[l];
				array[l] = temp;
			}
		}
		cout << array[k] << " ";
	}
	cout << endl;
}

int main()
{
	srand(time(NULL));
	int array[10];
;
	
	arraySort(array);

	system("pause");
	return 0;
}