#include <iostream>
#include <string>
#include <ctime>

using namespace std;

// bet function
void bet(int& gold, int& bet)
{
//bet check
	while (true)
	{
		cout << "Welcome to a random dice game!" << endl;
		cout << "Current Gold:" << gold << endl;
		cout << "Place the value of your bet:";
		cin >> bet;
		cout << "==============================================" << endl;
		if (bet <= 0)
		{
			cout << "Bet cannot be less than or equal to zero!" << endl;
		}
		else if (bet > gold)
		{
			cout << "Bet cannot be greater than current gold" << endl;
		}
		else
		{
			cout << "(void) Your bet is: " << bet << endl;
			break;
		}
		system("pause");
		system("cls");
	}
}
// AI rolls dice
void diceRoll(int(&dice)[2])
{
	dice[0] = rand() % 6 + 1;
	dice[1] = rand() % 6 + 1;
}
// dice print
void printArray(int dice[])
{
	for (int i = 0; i < 2; i++)
	{
		cout << dice[i] << " ";
	}
	cout << endl;
	system("pause");
}
//Roll Value
int diceValue(int(&dice)[2], int& value)
{
	diceRoll(dice);
	value = dice[0] + dice[1];
	return value;
}

//compare each value
void payout(int ai, int player, int& gold, int& bet)
{
	//check draw first
	if (ai == player)
	{
		cout << "It's a draw!" << endl;
		gold = gold;
	}
	//check snake eyes
	else if (ai == 2)
	{
		cout << "You lose!" << endl;
		gold -= bet;
	}
	else if (player == 2)
	{
		cout << "You got snake eyes! You won " << bet*3 << " gold!" << endl;
		gold = gold + (bet * 3);
	}
	//check win
	else if (player > ai)
	{
		cout << "You win " << bet << " gold!" << endl;
		gold += bet;
	}
	//check lose
	else if (player < ai)
	{
		cout << "You lose " << bet << " gold!" << endl;
		gold -= bet;
	}
	system("pause");
	system("cls");
}

int main()
{
	srand(time(NULL));
	int player;
	int ai;
	int playerGold = 1000;
	int playerDice[2];
	int aiDice[2];
	int playerBet;

	while (playerGold > 0)
	{
		bet(playerGold, playerBet);
		
		cout << "Ai Dice: " << endl;
		diceValue(aiDice, ai);
		printArray(aiDice);

		cout << "Player Dice: " << endl;
		diceValue(playerDice, player);
		printArray(playerDice);

		payout(ai, player, playerGold, playerBet);
		
	}
	system("pause");
	return 0;
}