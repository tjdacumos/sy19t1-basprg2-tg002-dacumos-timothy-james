#include <iostream>
#include <string>
#include <ctime>
#include <conio.h>

using namespace std;

// AI rolls dice
void diceRoll(int(&dice)[2])
{
	dice[0] = rand() % 6 + 1;
	dice[1] = rand() % 6 + 1;

}

// dice print
void printArray(int dice[])
{
	for (int i = 0; i < 2; i++)
	{
		cout << dice[i] << " ";
	}
}

int main()
{
	srand(time(NULL));

	int diceAI[2];
	int dicePlayer[2];

	diceRoll(diceAI);
	diceRoll(dicePlayer);
	cout << "AI Dice: " << endl;
	printArray(diceAI);
	cout << endl;
	system("pause");
	cout << "Player Dice: " << endl;
	printArray(dicePlayer);
	cout << endl;

	system("pause");
	return 0;
}

