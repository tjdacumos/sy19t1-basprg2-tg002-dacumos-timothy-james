#include <iostream>
#include <string>

using namespace std;

//Bet function v1
int betV1(int& gold)
{
	int bet = 0;
	while (true)
	{
		cout << "Int function" << endl;
		cout << "(Int) Current Gold:" << gold << endl;
		cout << "Place the value of your bet:";
		cin >> bet;
		cout << "==============================================" << endl;
		if (bet <= 0)
		{
			cout << "Bet too low! Place bet higher than 0." << endl;
		}
		else if (bet > gold)
		{
			cout << "Bet too high! Place bet lower than your money." << endl;
		}
		else
		{
			gold -= bet;
			cout << "(int) Your bet is: " << bet << endl;
			return bet;
		}
		system("pause");
		system("cls");
	}	
}
// Bet funciton v2
void betV2(int& gold, int& bet)
{
	
	while (true)
	{
		
		cout << "Void function" << endl;
		cout << "(Void) Current Gold:" << gold << endl;
		cout << "Place the value of your bet:";
		cin >> bet;
		cout << "==============================================" << endl;

		if (bet <= 0)
		{
			cout << "Bet cannot be less than or equal to zero!" << endl;
		}
		else if (bet > gold)
		{
			cout << "Bet cannot be greater than current gold" << endl;
		}
		else
		{
			gold -= bet;
			cout << "(void) Your bet is: " << bet << endl;
			break;
		}
		system("pause");
		system("cls");
	}
}

int main()
{
	int goldV1 = 1000;
	int goldV2 = 1000;
	int bet;

	while (goldV1 > 0 && goldV2 > 0)
	{
		
		betV1(goldV1);

		system("pause");
		system("cls");
		
		betV2(goldV2, bet);

		system("pause");
		system("cls");
	}

	system("pause");
	return 0;
}