#include <iostream>
#include <string>
#include <ctime>
#include <conio.h>

using namespace std;

//struct of item
struct Item
{
	string name;
	int gold;
}loot;
//function for generate random item (Ex 3-4)
Item generateItem() ////revised entire code after being taught!!
{
	Item *loot, random;
	loot = &random;
	string items[5] = { "Mithril Ore", "Sharp Talon", "Thick Leather", "Jellopy", "Cursed Stone" };
	int itemsGold[5] = { 100, 50, 25, 5, 0 };
	int i = rand() % 5;
	// Print roled item
	cout << "You got a " << items[i] << "!" << endl;
	(*loot).name = items[i];
	(*loot).gold = itemsGold[i];
	return *loot;
}
//function for enter  (Ex 3-5)
int enterDungeon(int* gold)
{
	string cont = "y";
	int multiplier = 1;
	int tempGold = 0;
	//deduct 25 gold automatically
	*gold -= 25;
	cout << "You have entered the dungeon!" << endl;
	system("pause");
	while (cont != "x")
	{
		loot = generateItem();
		// cursed stone special condition
		if ((loot).name == "Cursed Stone")
		{
			cout << "Oh no! You lost everything including your life... You auto exit the dungeon!" << endl;
			return *gold;
		}
		//print rolled gold of the item output
		cout << "You earned " << (loot).gold << " gold!" << endl;
		*gold += ((loot).gold * multiplier);

		system("pause");
		//mutliplier stuff
		tempGold += ((loot).gold * multiplier);
		if (multiplier < 4)
		{
			multiplier++;
		}
		cout << "\nTotal Earned gold this run is " << tempGold << " gold" << endl;
		cout << "Current multiplier x" << multiplier << endl;
		//continue or not
		cout << "\nDo you want to explore more? press 'x' to exit ";
		cont = _getch();
		cout << endl;
		system("pause");
		system("cls");
	}
	//when player exits dungeon
	cout << "You decided to exit the dungeon!" << endl;
	*gold += tempGold;
	return *gold;

}

//function for game condition (Ex 3-6)
void evaluateEnding(int* gold)
{
	//win condition
	if (*gold >= 500)
	{
		cout << "You're succesful in your dungeon crawl! You win!" << endl;
	}
	//lose condtion
	else
	{
		cout << "You can't enter the dungeon anymore because you don't have enough gold... You lose! :(" << endl;
	}
}

//Play everything
void playGame(int* gold)
{
	cout << "Welcome to a random dungeon crawl game!" << endl;
	while (*gold >= 25 && *gold < 500)
	{
		enterDungeon(gold);
		cout << "Your current gold: " << *gold << endl;
		system("pause");
		system("cls");
	}
	evaluateEnding(gold);
	
}

int main()
{
	srand(time(NULL));
	int gold = 50;
	int* goldUser;
	goldUser = &gold;

	playGame(goldUser);

	system("pause");
	return 0;
}