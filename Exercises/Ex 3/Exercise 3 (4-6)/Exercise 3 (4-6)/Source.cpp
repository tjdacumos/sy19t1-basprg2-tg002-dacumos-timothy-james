#include <iostream>
#include <string>
#include <ctime>

using namespace std;

//struct of item
struct Item
{
	string name;
	int gold;
};
//function for generate random item (Ex 3-4)
Item generateItem(string items[])
{
	Item random;
	int i = rand() % 5;

	if (i == 4)
	{
		cout << "You got a " << items[i] << "!" << endl;
		random.name = "Cursed Stone";
		random.gold = 0;
	}
	else if (i == 3)
	{
		cout << "You got a " << items[i] << "!" << endl;
		random.name = "Jellopy";
		random.gold = 5;
	}
	else if (i == 2)
	{
		cout << "You got a " << items[i] << "!" << endl;
		random.name = "Thick Leather";
			random.gold = 25;
	}
	else if (i == 1)
	{
		cout << "You got a " << items[i] << "!" << endl;
		random.name = "Sharp Talon";
		random.gold = 50;
	}
	else if (i == 0)
	{
		cout << "You got a " << items[i] << "!" << endl;
		random.name = "Mithril Ore";
		random.gold = 100;
	}
	return random;
}
//function for enter  (Ex 3-5)
int enterDungeon(int* gold, string items[])
{
	string cont;
	int multiplier = 1;
	int tempGold = 0;

	*gold -= 25;
	cout << "You have entered the dungeon!" << endl;
	system("pause");
	while (true)
	{
		Item random = generateItem(items);

		if (random.name == "Cursed Stone")
		{
			cout << "Oh no! You lost everything including your life... You automatically lose!" << endl;
			*gold = -1;
			return *gold;
		}
		else if (random.name == "Jellopy")
		{
			cout << "You earned " << random.gold << "!" << endl;
		}
		else if (random.name == "Thick Leather")
		{
			cout << "You earned " << random.gold << "!" << endl;
		}
		else if (random.name == "Sharp Talon")
		{
			cout << "You earned " << random.gold << "!" << endl;
		}
		else if (random.name == "Mithril Ore")
		{
			cout << "You earned " << random.gold << "!" << endl;
		}
		system("pause");
		tempGold += (random.gold * multiplier);
		if (multiplier < 5)
		{
			multiplier++;
		}
		cout << "\nTotal Earned gold this run is " << tempGold << " gold" << endl;
		cout << "Current multiplier x" << multiplier << endl;
		cout << "\nDo you want to explore more? (entering other keys/words means continue) (Y) Yes / (N) No: ";
		cin >> cont;
		if (cont == "No" || cont == "N" || cont == "n" || cont == "no")
		{
			break;
		}
		system("pause");
		system("cls");
	}
	*gold += tempGold;
	return *gold;

}

//function for game condition (Ex 3-6)
void evaluateEnding(int* gold)
{
	if (*gold >= 500)
	{
		cout << "You're succesful in your dungeon crawl! You win!" << endl;
	}
	else if (*gold == -1)
	{
		cout << "Better luck next time!" << endl;
	}
	else
	{
		cout << "You can't enter the dungeon anymore.. You lose! :(" << endl;
	}
}

//Play everything
void playGame(int* gold, string items[])
{
	cout << "Welcome to a random dungeon crawl game!" << endl;
	while (*gold >= 25 && *gold < 500)
	{
		enterDungeon(gold, items);
		if (*gold == -1)
		{
			break;
		}
		
		cout << "Your current gold: " << *gold << endl;
		system("pause");
		system("cls");
	}
	evaluateEnding(gold);
	
}

int main()
{
	srand(time(NULL));
	string items[5] = { "Mithril Ore", "Sharp Talon", "Thick Leather", "Jellopy", "Cursed Stone" };
	int *goldUser = new int;
	*goldUser = 50;

	playGame(goldUser, items);

	system("pause");
	return 0;
}