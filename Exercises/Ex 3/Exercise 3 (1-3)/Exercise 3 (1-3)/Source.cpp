#include <iostream>
#include <string>
#include <ctime>

using namespace std;

//Array as Pointer (Ex 3-1)
void arrayPoint(int* numbers, int size)
{
	for (int i = 0; i < size; i++)
	{
		numbers[i] = rand() % 100 + 1;
	}
	cout << "\nsize of numbers(pointer): " << sizeof(numbers) << endl;
}

//Array Printer 
void arrayPrint(int* numbers, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << numbers[i] << " ";
	}
}
//Dynamic Array (Ex 3-2)
int* arrayDynamic(int size)
{
	int* numbersDynamic;
	numbersDynamic = new int[size];

	for (int i = 0; i < size; i++)
	{
		numbersDynamic[i] = rand() % 100 + 1;	
	}
	cout << "\nsize of numbers (dynamic): " << sizeof(numbersDynamic) << endl;
	return numbersDynamic;
}

//Deleting dynamic elements (Ex 3-3)
void arrayDelete(int* numbers)
{
	delete []numbers;
	numbers = 0;
}

int main()
{
	srand(time(NULL));

	int numbersPoint[5];
	int size;
	int *temp;

	cout << "Array as Pointer" << endl;
	arrayPoint(numbersPoint, 5);
	cout << "size of numbers(non - pointer): " << sizeof(numbersPoint) << endl;
	arrayPrint(numbersPoint, 5);
	cout << endl;

	cout << "\nDynamic Memory" << endl;
	cout << "Enter size of array: ";
	cin >> size;
	temp = arrayDynamic(size);
	arrayPrint(temp, size);
	
	cout << "\n\nDelete Array" << endl;
	arrayDelete(temp);
	cout << "Array deleted!" << endl;

	system("pause");
	return 0;
	
}
